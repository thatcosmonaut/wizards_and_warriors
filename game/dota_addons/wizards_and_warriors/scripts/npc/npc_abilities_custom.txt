
// Dota Abilities Override File
"DOTAAbilities"
{
  "Version"   "1"

  "containers_lua_targeting"
  {
    // General
    //-------------------------------------------------------------------------------------------------------------
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_UNIT_TARGET | DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_HIDDEN"
    "AbilityUnitTargetTeam"     "DOTA_UNIT_TARGET_TEAM_BOTH"
    //"AbilityUnitTargetType"     "DOTA_UNIT_TARGET_ALL"
    "AbilityUnitTargetType"     "DOTA_UNIT_TARGET_ALL"
    "AbilityUnitDamageType"     "DAMAGE_TYPE_MAGICAL"
    "BaseClass"         "ability_lua"
    "AbilityTextureName"        "rubick_empty1"
    "ScriptFile"          "libraries/abilities/containers_lua_targeting"
    "MaxLevel"            "1"
    "IsCastableWhileHidden" "1"

    // Casting
    //-------------------------------------------------------------------------------------------------------------
    "AbilityCastRange"        "0"
    "AbilityCastPoint"        "0.0"

    // Time
    //-------------------------------------------------------------------------------------------------------------
    "AbilityCooldown"       "0"

    // Cost
    //-------------------------------------------------------------------------------------------------------------
    "AbilityManaCost"       "0 0 0 0"
  }

  "containers_lua_targeting_tree"
  {
    // General
    //-------------------------------------------------------------------------------------------------------------
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_UNIT_TARGET | DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_HIDDEN"
    "AbilityUnitTargetTeam"     "DOTA_UNIT_TARGET_TEAM_BOTH"
    //"AbilityUnitTargetType"     "DOTA_UNIT_TARGET_ALL"
    "AbilityUnitTargetType"     "DOTA_UNIT_TARGET_ALL | DOTA_UNIT_TARGET_TREE"
    "AbilityUnitDamageType"     "DAMAGE_TYPE_MAGICAL"
    "BaseClass"         "ability_lua"
    "AbilityTextureName"        "rubick_empty1"
    "ScriptFile"          "libraries/abilities/containers_lua_targeting_tree"
    "MaxLevel"            "1"
    "IsCastableWhileHidden" "1"

    // Casting
    //-------------------------------------------------------------------------------------------------------------
    "AbilityCastRange"        "0"
    "AbilityCastPoint"        "0.0"

    // Time
    //-------------------------------------------------------------------------------------------------------------
    "AbilityCooldown"       "0"

    // Cost
    //-------------------------------------------------------------------------------------------------------------
    "AbilityManaCost"       "0 0 0 0"
  }

  "example_ability"
  {
    "ID"              "1852"
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_NO_TARGET | DOTA_ABILITY_BEHAVIOR_CHANNELLED"
    "BaseClass"           "ability_datadriven"
    "AbilityTextureName"      "holdout_blade_fury"

    // Stats
    //-------------------------------------------------------------------------------------------------------------
    "AbilityCastRange"        "1000"
    "AbilityCastPoint"        "0.0"
    "AbilityCooldown"       "10.0"
    "AbilityChannelTime"      "2.0 1.8 1.6 1.5"
    "AbilityUnitDamageType"     "DAMAGE_TYPE_PURE"
    "AbilityCastAnimation"      "ACT_DOTA_DISABLED"
    "AbilityDamage"         "400 600 800 1000"

    "precache"
    {
      "particle"          "particles/econ/generic/generic_aoe_explosion_sphere_1/generic_aoe_explosion_sphere_1.vpcf"
      "soundfile"         "soundevents/game_sounds_heroes/game_sounds_gyrocopter.vsndevts"
    }

    // Item Info
    //-------------------------------------------------------------------------------------------------------------
    "AbilityManaCost"       "300"
    "SideShop"            "1"

    "OnSpellStart"
    {
      "ApplyModifier"
      {
        "Target"    "CASTER"
        "ModifierName"  "modifier_channel_start"
      }
      "FireSound"
      {
        "EffectName"    "Hero_Gyrocopter.CallDown.Fire"
        "Target"      "CASTER"
      }
    }

    "OnChannelSucceeded"
    {
      "RemoveModifier"
      {
        "Target"        "CASTER"
        "ModifierName"      "modifier_channel_start"
      }
      "AttachEffect"
      {
        "EffectName"      "particles/econ/generic/generic_aoe_explosion_sphere_1/generic_aoe_explosion_sphere_1.vpcf"
        "EffectAttachType"    "follow_origin"
        "EffectRadius"      "%radius"
        "EffectDurationScale" "1"
        "EffectLifeDurationScale" "1"
        "EffectColorA"      "255 0 0"
        "EffectColorB"      "255 0 0"
        "Target"      "CASTER"
      }

      "Damage"
      {
        "Type"          "DAMAGE_TYPE_PURE"
        "Damage"        "%damage"
        "Target"
        {
          "Center"      "CASTER"
          "Radius"      "%radius"
          "Teams"       "DOTA_UNIT_TARGET_TEAM_ENEMY"
          "Types"       "DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
        }
      }

      "Knockback"
      {
        "Center"  "CASTER"
        "Target"
        {
          "Center"  "CASTER"
          "Radius"  "%radius"
          "Teams"   "DOTA_UNIT_TARGET_TEAM_ENEMY"
        }
        "Duration"  "%duration"
        "Distance"  "%distance"
        "Height"  "%height"
      }

      "FireSound"
      {
        "EffectName"    "Hero_Gyrocopter.CallDown.Damage"
        "Target"      "CASTER"
      }
    }

    "OnChannelFinish"
    {
      "RemoveModifier"
      {
        "Target"        "CASTER"
        "ModifierName"      "modifier_channel_start"
      }
    }

    "OnChannelInterrupted"
    {
      "RemoveModifier"
      {
        "Target"    "CASTER"
        "ModifierName"  "modifier_channel_start"
      }
    }

    "Modifiers"
    {
      "modifier_channel_start"
      {
        "OnCreated"
        {
          "AttachEffect"
          {
            "IsHidden" "1"
            "EffectName"    "particles/test_particle/channel_field_2.vpcf"//"gyro_calldown_marker_c"//"gyrocopter_call_down"
            "EffectAttachType"  "follow_origin"
            "Target"      "CASTER"

            "EffectRadius"      "%radius"
            "EffectColorA"      "255 0 0"
            "EffectColorB"      "255 0 0"

            "ControlPoints"
            {
              "00"    "50 100 5"
            }
          }
        }
      }
    }

    // Special
    //-------------------------------------------------------------------------------------------------------------
    "AbilitySpecial"
    {
      "01"
      {
        "var_type"        "FIELD_FLOAT"
        "duration"        "0.5"
      }

      "02"
      {
        "var_type"        "FIELD_INTEGER"
        "damage"        "400 600 800 1000"
      }

      "03"
      {
        "var_type"        "FIELD_INTEGER"
        "radius"        "550 550 600 650"
      }

      "04"
      {
        "var_type"        "FIELD_INTEGER"
        "distance"        "400 500 600 700"
      }

      "05"
      {
        "var_type"        "FIELD_INTEGER"
        "height"        "100 200 300 400"
      }
    }
  }

//-------------------
// Dummy Sleep

  "dummy_sleep_ability"
  {
    "BaseClass"         "ability_lua"
    "ScriptFile"        "dummy_sleep_ability"
  }

//-------------------

//-------------------
// Dummy stuff
//-------------------

  "dummy_unit"
  {
    "AbilityBehavior"     "DOTA_ABILITY_BEHAVIOR_PASSIVE"
    "BaseClass"           "ability_datadriven"
    //"AbilityTextureName"            "rubick_empty1"
    //"MaxLevel"                      "1"

    "Modifiers"
    {
      "dummy_unit"
      {
        "Passive"                        "1"
        //"IsHidden"                        "1"
        "States"
        {
          "MODIFIER_STATE_NO_UNIT_COLLISION"	"MODIFIER_STATE_VALUE_ENABLED"
  				"MODIFIER_STATE_NO_TEAM_MOVE_TO"	"MODIFIER_STATE_VALUE_ENABLED"
  				"MODIFIER_STATE_NO_TEAM_SELECT"	"MODIFIER_STATE_VALUE_ENABLED"
  				"MODIFIER_STATE_COMMAND_RESTRICTED"	"MODIFIER_STATE_VALUE_ENABLED"
  				"MODIFIER_STATE_ATTACK_IMMUNE"	"MODIFIER_STATE_VALUE_ENABLED"
  				"MODIFIER_STATE_INVULNERABLE"	"MODIFIER_STATE_VALUE_ENABLED"
  				//"MODIFIER_STATE_FLYING"	"MODIFIER_STATE_VALUE_ENABLED"
  				"MODIFIER_STATE_NOT_ON_MINIMAP"	"MODIFIER_STATE_VALUE_ENABLED"
  				"MODIFIER_STATE_UNSELECTABLE"	"MODIFIER_STATE_VALUE_ENABLED"
  				"MODIFIER_STATE_OUT_OF_GAME"	"MODIFIER_STATE_VALUE_ENABLED"
  				"MODIFIER_STATE_NO_HEALTH_BAR"		   "MODIFIER_STATE_VALUE_ENABLED"
        }
      }
    }
  }

  "barebones_empty1"
  {
    // General
    //-------------------------------------------------------------------------------------------------------------
    //"ID"              "5343"                            // unique ID number for this ability.  Do not change this once established or it will invalidate collected stats.
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_NOT_LEARNABLE"
    "MaxLevel"            "0"
    "BaseClass"           "ability_datadriven"
    "AbilityTextureName"            "rubick_empty1"
  }

  "barebones_empty2"
  {
    // General
    //-------------------------------------------------------------------------------------------------------------
    //"ID"              "5343"                            // unique ID number for this ability.  Do not change this once established or it will invalidate collected stats.
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_NOT_LEARNABLE"
    "MaxLevel"            "0"
    "BaseClass"           "ability_datadriven"
    "AbilityTextureName"            "rubick_empty1"
  }

  "barebones_empty3"
  {
    // General
    //-------------------------------------------------------------------------------------------------------------
    //"ID"              "5343"                            // unique ID number for this ability.  Do not change this once established or it will invalidate collected stats.
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_NOT_LEARNABLE"
    "MaxLevel"            "0"
    "BaseClass"           "ability_datadriven"
    "AbilityTextureName"            "rubick_empty1"
  }

  "barebones_empty4"
  {
    // General
    //-------------------------------------------------------------------------------------------------------------
    //"ID"              "5343"                            // unique ID number for this ability.  Do not change this once established or it will invalidate collected stats.
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_NOT_LEARNABLE"
    "MaxLevel"            "0"
    "BaseClass"           "ability_datadriven"
    "AbilityTextureName"            "rubick_empty1"
  }

  "barebones_empty5"
  {
    // General
    //-------------------------------------------------------------------------------------------------------------
    //"ID"              "5343"                            // unique ID number for this ability.  Do not change this once established or it will invalidate collected stats.
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_NOT_LEARNABLE"
    "MaxLevel"            "0"
    "BaseClass"           "ability_datadriven"
    "AbilityTextureName"            "rubick_empty1"
  }

  "barebones_empty6"
  {
    // General
    //-------------------------------------------------------------------------------------------------------------
    //"ID"              "5343"                            // unique ID number for this ability.  Do not change this once established or it will invalidate collected stats.
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_NOT_LEARNABLE"
    "MaxLevel"            "0"
    "BaseClass"           "ability_datadriven"
    "AbilityTextureName"            "rubick_empty1"
  }

  "crystal_maiden_brilliance_aura"
  {
    "AbilitySpecial"
    {
      "01"
      {
        "var_type"         "FIELD_FLOAT"
        "mana_regen"       "3.0 5.0 7.5 10.0"
      }
      "02"
      {
        "var_type"			"FIELD_FLOAT"
        "mana_regen_self"	"3.0 5.0 7.5 10.0"
      }
    }
  }

  "ancient_apparition_chilling_touch"
  {
    "AbilityBehavior"     "DOTA_ABILITY_BEHAVIOR_UNIT_TARGET | DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING"
    "AbilityUnitTargetTeam"   "DOTA_UNIT_TARGET_TEAM_FRIENDLY"
    "AbilityUnitTargetType"   "DOTA_UNIT_TARGET_HERO"
    "AbilitySpecial"
    {
      "01"
      {
        "var_type"				"FIELD_INTEGER"
        "radius"				"0"
      }
      "02"
      {
        "var_type"				"FIELD_INTEGER"
        "max_attacks"			"3 4 5 6"
      }
      "03"
      {
        "var_type"				"FIELD_INTEGER"
        "bonus_damage"			"50 60 70 80"
      }
      "04"
      {
        "var_type"				"FIELD_FLOAT"
        "attack_speed_pct"		"-20"
      }
    }
  }

  "juggernaut_omni_slash"
  {
  	"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"

    "AbilitySpecial"
  	{
  		"01"
  		{
  			"var_type"					"FIELD_INTEGER"
  			"omni_slash_damage"			"125 150"
  		}
  		"02"
  		{
  			"var_type"					"FIELD_INTEGER"
  			"omni_slash_jumps"			"3 5 7"
  		}
  		"03"
  		{
  			"var_type"					"FIELD_INTEGER"
  			"omni_slash_radius"			"70"
  		}
  		"04"
  		{
  			"var_type"					"FIELD_FLOAT"
  			"omni_slash_bounce_tick"	"0.1"
  		}
  		"05"
  		{
  			"var_type"						"FIELD_INTEGER"
  			"omni_slash_jumps_scepter"		"6 9 12"
  		}
  		"06"
  		{
  			"var_type"						"FIELD_FLOAT"
  			"omni_slash_cooldown_scepter"	"70"
  		}
  	}
  }

  "slardar_channel_strength"
  {
    "BaseClass"   "ability_datadriven"
    "AbilityTextureName"    "slardar_sprint"

    "AbilityBehavior"   "DOTA_ABILITY_BEHAVIOR_NO_TARGET | DOTA_ABILITY_BEHAVIOR_IMMEDIATE | DOTA_ABILITY_BEHAVIOR_IGNORE_CHANNEL"
    "AbilityCastRange"      "0"
    "AbilityCastPoint"      "0.0 0.0 0.0 0.0"
    "AbilityCooldown"       "17"

    "AbilitySpecial"
    {
        "01"
        {
          "var_type"    "FIELD_INTEGER"
          "attack_speed_bonus"    "10 15 20 30"
        }

        "02"
        {
          "var_type"    "FIELD_INTEGER"
          "incoming_damage_increase"    "5 7.5 10 15"
        }

        "03"
        {
          "var_type"     "FIELD_INTEGER"
          "duration"     "10"
        }
    }

    "OnSpellStart"
    {
      "ApplyModifier"
      {
        "ModifierName"  "modifier_slardar_channel_strength"
        "Target"    "CASTER"
      }
    }

    "Modifiers"
    {
      "modifier_slardar_channel_strength"
      {
        "Duration"    "%duration"
        "IsHidden"    "0"
        "IsBuff"      "1"

        "Properties"
        {
          "MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT"    "%attack_speed_bonus"
          "MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE"    "%incoming_damage_increase"
        }
      }
    }
  }

  "explosive_tome_explode"
  {
    "BaseClass"       "ability_datadriven"
    "AbilityBehavior"   "DOTA_ABILITY_BEHAVIOR_NO_TARGET | DOTA_ABILITY_BEHAVIOR_CHANNELLED"
    "AbilityTextureName"    "spellicon"

    "AbilityChannelTime"      "40"

    "AbilityUnitTargetTeam"    "DOTA_UNIT_TARGET_TEAM_BOTH"
    "AbilityUnitDamageType"   "DAMAGE_TYPE_PURE"
    "AbilityUnitTargetType"   "DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
    "SpellImmunityType"       "SPELL_IMMUNITY_ENEMIES_YES"

    "precache"
    {
      "soundfile"       "soundevents/game_sounds_heroes/game_sounds_techies.vsndevts"
      "particle"        "particles/units/heroes/hero_techies/techies_remote_mines_detonate.vpcf"
    }

    "OnAbilityPhaseStart"
    {
      "RunScript"
      {
        "ScriptFile"    "scripts/vscripts/items/explosive_tome.lua"
        "Function"      "explosive_tome_plant"
        "Target"        "CASTER"
      }
    }

    "OnSpellStart"
    {
      "FireSound"
		    {
			       "EffectName"	"Hero_Techies.RemoteMine.Plant"
			          "Target" 		"CASTER"
		    }
    }

    "OnChannelSucceeded"
    {
      "FireSound"
      {
        "EffectName"    "Hero_Techies.RemoteMine.Detonate"
        "Target"
        {
          "Center"      "CASTER"
          "Flags"       "DOTA_UNIT_TARGET_FLAG_DEAD"
        }
      }

      "FireEffect"
      {
        "EffectName"        "particles/units/heroes/hero_techies/techies_remote_mines_detonate.vpcf"
        "EffectAttachType"  "follow_hitloc"
        "Target"
        {
          "Center"	"CASTER"
          "Flags"		"DOTA_UNIT_TARGET_FLAG_DEAD"
        }
      }

      "RunScript"
      {
        "ScriptFile"      "scripts/vscripts/items/explosive_tome.lua"
        "Function"        "explosive_tome_explode"
        "Target"          "CASTER"
      }
    }

    "OnChannelInterrupted"
    {
      "RunScript"
      {
        "ScriptFile"      "scripts/vscripts/items/explosive_tome.lua"
        "Function"        "explosive_tome_defuse"
        "Target"          "CASTER"
      }
    }
  }

  "explosive_tome_regenerate"
  {
    "BaseClass"         "ability_datadriven"
    "AbilityBehavior"     "DOTA_ABILITY_BEHAVIOR_PASSIVE"
    "AbilityTextureName"    "spellicon"

    "Modifiers"
    {
      "modifier_explosive_tome_regenerate"
      {
        "Passive"     "1"
        "IsHidden"    "1"

        "OnCreated"
        {
          "ApplyModifier"
          {
            "ModifierName"    "modifier_explosive_tome_regenerate_on"
            "Target"        "CASTER"
          }
        }
      }

      "modifier_explosive_tome_regenerate_on"
      {
        "Passive"     "0"
        "IsHidden"    "0"

        "OnTakeDamage"
        {
          "ApplyModifier"
          {
            "ModifierName"    "modifier_explosive_tome_regenerate_off"
            "Target"      "CASTER"
          }
        }
      }

      "modifier_explosive_tome_regenerate_off"
      {
        "Passive"     "0"
        "IsBuff"      "0"
        "IsHidden"    "0"

        "Duration"      "5"

        "Properties"
        {
          "MODIFIER_PROPERTY_DISABLE_HEALING"   "1"
        }

        "OnTakeDamage"
        {
          "ApplyModifier"
          {
            "ModifierName"  "modifier_explosive_tome_regenerate_off"
            "Target"      "CASTER"
          }
        }

        "OnDestroy"
        {
          "ApplyModifier"
          {
            "ModifierName"    "modifier_explosive_tome_regenerate_on"
            "Target"      "CASTER"
          }
        }
      }
    }

  }

  "explosive_tome_modifier"
  {
    "BaseClass"         "ability_datadriven"
    "AbilityBehavior"     "DOTA_ABILITY_BEHAVIOR_PASSIVE"
    "AbilityTextureName"    "spellicon"

    "Modifiers"
    {
      "modifier_explosive_tome_magic_immune"
      {
        "Passive"     "1"
        "IsHidden"    "1"

        "States"
        {
          "MODIFIER_STATE_MAGIC_IMMUNE" "MODIFIER_STATE_VALUE_ENABLED"
          "MODIFIER_STATE_INVULNERABLE" "MODIFIER_STATE_VALUE_ENABLED"
        }
      }
    }
  }

  // Rewrite of the Axe Culling Blade ability
  // Author: Pizzalol
  // Date: 14.10.2015.
  // Version: 6.85
  // Type: Datadriven
  // NOTE: You need to specify the modifiers that need to be removed (only if they cant be purged)
  // which would otherwise prevent death in the culling_blade.lua file
  //
  // ----- FILE REQUIREMENTS -----
  // Script files:
  // scripts/vscripts/heroes/hero_axe/culling_blade.lua
  "legion_commander_execution_strike"
  {
  	// General
  	//-------------------------------------------------------------------------------------------------------------
  	"BaseClass"				"ability_datadriven"
  	"AbilityType"					"DOTA_ABILITY_TYPE_ULTIMATE"
  	"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_UNIT_TARGET"
  	"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
  	"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
  	"AbilityUnitTargetFlags"		"DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES"
  	"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_YES"
  	"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"
  	"FightRecapLevel"				"2"
  	"AbilityTextureName"			"legion_commander_duel"
    "AbilityCastAnimation"       "ACT_DOTA_ATTACK"

  	// Precache
  	//-------------------------------------------------------------------------------------------------------------
  	"precache"
  	{
  		"soundfile"			"soundevents/game_sounds_heroes/game_sounds_axe.vsndevts"
  		"particle"			"particles/units/heroes/hero_axe/axe_culling_blade.vpcf"
  		"particle"			"particles/units/heroes/hero_axe/axe_culling_blade_boost.vpcf"
  		"particle"			"particles/units/heroes/hero_axe/axe_culling_blade_kill.vpcf"
  		"particle"			"particles/units/heroes/hero_axe/axe_cullingblade_sprint.vpcf"
  	}

  	// Casting
  	//-------------------------------------------------------------------------------------------------------------
  	"AbilityCastPoint"				"0.3 0.3 0.3"

  	// Time
  	//-------------------------------------------------------------------------------------------------------------
  	"AbilityCooldown"				"75.0 65.0 55.0"

  	// Cost
  	//-------------------------------------------------------------------------------------------------------------
  	"AbilityManaCost"				"60 120 180"

  	// Cast Range
  	//-------------------------------------------------------------------------------------------------------------
  	"AbilityCastRange"				"150"

  	// Special
  	//-------------------------------------------------------------------------------------------------------------
  	"AbilitySpecial"
  	{
  		"01"
  		{
  			"var_type"					"FIELD_INTEGER"
  			"kill_threshold"			"400 550 700"
  		}
  		"02"
  		{
  			"var_type"					"FIELD_INTEGER"
  			"damage"					"100 150 200"
  		}
  		"03"
  		{
  			"var_type"					"FIELD_INTEGER"
  			"speed_bonus"				"30"
  		}
  		"04"
  		{
  			"var_type"					"FIELD_FLOAT"
  			"speed_duration"			"6"
  		}
  		"05"
  		{
  			"var_type"					"FIELD_INTEGER"
  			"speed_aoe"					"900"
  		}
  		"06"
  		{
  			"var_type"					"FIELD_FLOAT"
  			"cooldown_scepter"			"6.0 6.0 6.0"
  		}
  		"07"
  		{
  			"var_type"					"FIELD_INTEGER"
  			"kill_threshold_scepter"	"500 650 800"
  		}
  		"08"
  		{
  			"var_type"					"FIELD_FLOAT"
  			"speed_duration_scepter"	"10"
  		}
  	}

  	"OnSpellStart"
  	{
  		"FireEffect"
  		{
  			"EffectName"        "particles/units/heroes/hero_axe/axe_culling_blade.vpcf"
  			"EffectAttachType"  "follow_origin"
  			"Target"            "TARGET"
  		}

  		"RunScript"
  		{
  			"ScriptFile"		"heroes/hero_legion_commander/execution_strike.lua"
  			"Function"			"ExecutionStrike"
  			"sound_fail"		"Hero_Axe.Culling_Blade_Fail"
  			"sound_success"		"Hero_Axe.Culling_Blade_Success"
  			"particle_kill"		"particles/units/heroes/hero_axe/axe_culling_blade_kill.vpcf"
  			"modifier_sprint"	"modifier_culling_blade_sprint_datadriven"
  		}
  	}

  	"Modifiers"
  	{
  		"modifier_culling_blade_sprint_datadriven"
  		{
  			"IsBuff"	"1"

  			"EffectName"		"particles/units/heroes/hero_axe/axe_cullingblade_sprint.vpcf"
  			"EffectAttachType"	"follow_origin"

  			"OnCreated"
  			{
  				"FireEffect"
  				{
  					"EffectName"        "particles/units/heroes/hero_axe/axe_culling_blade_boost.vpcf"
  					"EffectAttachType"  "start_at_customorigin"

  					"ControlPointEntities"
  					{
  						"TARGET"	"follow_origin"
  						"TARGET"	"follow_origin"
  					}
  				}
  			}

  			"Properties"
  			{
  				"MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT" "%speed_bonus"
  			    "MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE" "%speed_bonus"
  			}
  		}
  	}
  }
}
