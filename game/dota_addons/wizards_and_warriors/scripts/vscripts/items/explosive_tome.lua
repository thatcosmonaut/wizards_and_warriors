function check_location(keys)
  local inTrigger = false
  local CHECK_RADIUS = 10

  local caster = EntIndexToHScript(keys.caster_entindex)
  local player_id = caster:GetPlayerID()

  for _, trigger in pairs(Entities:FindAllByName("trigger_tome_placeable")) do
    if (trigger:IsTouching(caster)) then
      inTrigger = true
    end
  end

  if not inTrigger then
    caster:Stop()
    Notifications:Bottom(PlayerResource:GetPlayer(player_id), {text="Invalid Location For Tome.", duration=2.0})
  end
end

function spawn(keys)
  local caster = EntIndexToHScript(keys.caster_entindex)
  local team_number = caster:GetTeamNumber()
  local placement_position = GetGroundPosition(caster:GetOrigin(), nil)

  local h = CreateUnitByName("npc_explosive_tome", placement_position, true, nil, nil, team_number)
  local explode_ability = h:GetAbilityByIndex(1)

  local castOrder =
  {
    UnitIndex = h:entindex(),
    OrderType =  DOTA_UNIT_ORDER_CAST_NO_TARGET,
    TargetIndex = nil,
    AbilityIndex = explode_ability:entindex(),
    Position = nil,
    Queue = true
  }
  ExecuteOrderFromTable(castOrder)

  caster:RemoveItem(keys.ability)

  beep(h,  2, 0)
end

function beep(tome, seconds_to_next_beep, total_time)
  emit_beep_from_tome(tome)
  total_time = total_time + seconds_to_next_beep
  if total_time < 40 then
    seconds_to_next_beep = math.max(seconds_to_next_beep - ((2 / (total_time + 39)) + 0.02), 0.15)
    tome.beep_timer = Timers:CreateTimer(seconds_to_next_beep, function()
      beep(tome, seconds_to_next_beep, total_time)
    end)
  end
end

function emit_beep_from_tome(tome)
  for nPlayerID = 0, DOTA_MAX_TEAM_PLAYERS-1 do
    if PlayerResource:HasSelectedHero(nPlayerID) then
      local hero = PlayerResource:GetSelectedHeroEntity(nPlayerID)
      local distance = (hero:GetAbsOrigin() - tome:GetAbsOrigin()):Length2D()

      if (distance < 2000) then
        if (distance > 1100) then
          EmitSoundOnClient("NPC_ExplosiveTome.Explode.QuietBeep", hero:GetPlayerOwner())
        elseif (distance > 600) then
          EmitSoundOnClient("NPC_ExplosiveTome.Explode.MediumBeep", hero:GetPlayerOwner())
        else
          EmitSoundOnClient("NPC_ExplosiveTome.Explode.LoudBeep", hero:GetPlayerOwner())
        end
      end
    end
  end
end

function explosive_tome_plant(keys)
  Notifications:BottomToAll({text="The Tome has been planted.", duration=3.0})
  local tome = EntIndexToHScript(keys.caster_entindex)
  GameRules.GameMode:_TomePlanted(tome)
end

function explosive_tome_explode(keys)
  local ability = keys.ability
  local caster = EntIndexToHScript(keys.caster_entindex)
  local abilityDamage = 99999
  local targetTeam = ability:GetAbilityTargetTeam()
  local targetType = ability:GetAbilityTargetType()
  local targetFlag = ability:GetAbilityTargetFlags()
  local damageType = ability:GetAbilityDamageType()

  local attackPoint = caster:GetOrigin()

  -- Apply damage
  local units_in_radius = FindUnitsInRadius(caster:GetTeamNumber(), attackPoint, caster, 2000, targetTeam, targetType, targetFlag, 0, false)
  for k, v in pairs(units_in_radius) do
    local damageTable =
    {
      victim = v,
      attacker = caster,
      damage = abilityDamage,
      damage_type = damageType
    }
    ApplyDamage(damageTable)
  end

  GameRules.GameMode:_TomeExploded(caster:GetTeam())

  caster:ForceKill(false)
end

function explosive_tome_defuse(keys)
  local tome = EntIndexToHScript(keys.caster_entindex)
  GameRules.GameMode:_TomeDefused(tome:GetTeam())
  if Timers.timers[tome.beep_timer] ~= nil then
    Timers:RemoveTimer(tome.beep_timer)
  end
end
